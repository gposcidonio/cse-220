#!/usr/bin/env bash
case "$1" in
	all)
		find . -name "*.class" -type f | xargs rm -f
		javac $(find ./mars/* | grep .java)
		javac Mars.java
		jar cmf mainclass.txt Mars.jar PseudoOps.txt Config.properties Syscall.properties Settings.properties MARSlicense.txt mainclass.txt CreateMarsJar.bat CreateMarsJar.sh make.sh Mars.java Mars.class docs help images mars 
		echo "Run the command 'java -jar Mars.jar' to start Mars"
		;;
	build)
		javac $(find ./mars/* | grep .java)
		javac Mars.java
		;;
	clean)
		rm -f Mars.jar
		find . -name "*.class" -type f | xargs rm -f
		;;
	package)
		jar cmf mainclass.txt Mars.jar PseudoOps.txt Config.properties Syscall.properties Settings.properties MARSlicense.txt mainclass.txt CreateMarsJar.bat CreateMarsJar.sh make.sh Mars.java Mars.class docs help images mars 
		;;
	*)
		echo "Usage: $0 {all|build|clean|package}"
        exit 1
esac

