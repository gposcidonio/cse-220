package mars.mips.instructions.syscalls;

import mars.ProcessingException;
import mars.ProgramStatement;
import mars.mips.hardware.RegisterFile;
import mars.util.Binary;
import mars.util.SystemIO;

/**
 *
 * @author paul
 */
public class SyscallPrint16bit2Complement extends AbstractSyscall {
    public SyscallPrint16bit2Complement() {
        super(102, "SyscallPrint16bit2Complement");
    }
    
    /**
      * Performs syscall function to print on the console the integer stored in $a0, 
      * as the value of a 16-bit 2's complement number.
      */
    @Override
    public void simulate(ProgramStatement statement) throws ProcessingException {
        Integer value = RegisterFile.getValue(4);
        
        Integer sign = Binary.bitValue(value, 15);
        // Print the negative sign if the sign bit is set
        if(sign == 1) {
            // Sign extend the value
            value = value | 0xFFFF0000;
        } else {
            // Clear out the upper 16 bits 
            value = value & 0x0000FFFF;
        }
        SystemIO.printString(value.toString());
    }
}
