package mars.mips.instructions.syscalls;

import mars.ProcessingException;
import mars.ProgramStatement;
import mars.mips.hardware.RegisterFile;
import mars.util.SystemIO;

/**
 * System call for printing a 1's complement number in Mars.
 * @author Paul Campbell
 */
public class SyscallPrint1Complement extends AbstractSyscall {
    
    public SyscallPrint1Complement() {
        super(100, "Print1Complement");
    }
    
    /**
      * Performs syscall function to print on the console the integer stored in $a0, 
      * as the value of a one's complement number.
      */
    @Override
    public void simulate(ProgramStatement statement) throws ProcessingException {
        Integer value = RegisterFile.getValue(4);
        if(value < 0) {
            value += 1;
        }
        SystemIO.printString(value.toString());
    }
}
