package mars.mips.instructions.syscalls;

import mars.ProcessingException;
import mars.ProgramStatement;
import mars.mips.hardware.RegisterFile;
import mars.util.Binary;
import mars.util.SystemIO;

/**
 * System Call for printing a signed magnitude number in Mars.
 * @author Paul Campbell
 */
public class SyscallPrintSignedMagnitude extends AbstractSyscall {
    public SyscallPrintSignedMagnitude() {
        super(101, "PrintSignedMagnitude");
    }
    
    /**
      * Performs syscall function to print on the console the integer stored in $a0, 
      * as the value of a signed magnitude number.
      */
    @Override
    public void simulate(ProgramStatement statement) throws ProcessingException {
        Integer value = RegisterFile.getValue(4);
        
        Integer sign = Binary.bitValue(value, 31);
        // Print the negative sign if the sign bit is set
        if(sign == 1) {
            SystemIO.printString("-");
        }
        // Print out the rest of the bits
        Integer bits = value & 0x7FFFFFFF;
        SystemIO.printString(bits.toString());
    }
}
