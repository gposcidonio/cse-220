# Do NOT modify this file.
# This file is NOT part of your homework 2 submission.
.data
str_input: .asciiz "Input: "
str_result: .asciiz "Result: "

# strlen
strlen_header: .asciiz "\n\n********* strlen *********\n"
str_helloworld: .asciiz "This is not a palindrome"
str_abc: .asciiz "abc\n"

# strcmp
strcmp_header: .asciiz "\n\n********* strcmp *********\n"
strcmp_abc: .asciiz "abc"
strcmp_ABCD: .asciiz "ABCD"

# strncpy
strncpy_header: .asciiz "\n\n********* strncpy *********\n"
strncpy_buffer: .asciiz "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVW"

# indexof
indexof_header: .asciiz "\n\n********* indexof *********\n"

# reverse_str
reverse_header: .asciiz "\n\n********* reverse_str *********\n"

# atoi
atoi_header: .asciiz "\n\n********* atoi *********\n"
atoi_str: .asciiz "220"
atoi_12b5: .asciiz "12b5"

# cut
cut_header: .asciiz "\n\n********* cut *********\n"
cut_awesome: .asciiz "cacacashello"
cut_pattern_start: .asciiz "acas"
cut_pattern_end: .asciiz "llo"

# strtok
strtok_header: .asciiz "\n\n********* strtok *********\n"
strtok_awesome: .asciiz "Awesome ANSI escape: \\033[1 ;;;;;;; 45mHello, World!\\033[1 ; 40m Yay! ; Whazzap"

# Constants
.eqv QUIT 10
.eqv PRINT_STRING 4
.eqv PRINT_INT 1
.eqv NULL 0x0

.macro print_string(%address)
	li $v0, PRINT_STRING
	la $a0, %address
	syscall 
.end_macro

.macro print_newline
	li $v0, 11
	li $a0, '\n'
	syscall 
.end_macro

.macro print_space
	li $v0, 11
	li $a0, ' '
	syscall 
.end_macro

.macro print_int(%register)
	li $v0, 1
	add $a0, $zero, %register
	syscall
.end_macro

.text
.globl main

main:

	# TEST CASE for strlen	
	print_string(strlen_header)
	print_string(str_input)
	print_string(str_helloworld)
	print_newline

	la $a0, str_helloworld
	jal strlen

	move $t0, $v0
	print_string(str_result)	
	print_int($t0)
	print_newline

	print_string(str_input)
	print_string(str_abc)
	print_newline

	la $a0, str_abc
	jal strlen

	move $t0, $v0
	print_string(str_result)
	print_int($t0)
	print_newline

	# TEST CASE for strcmp
	print_string(strcmp_header)
	print_string(str_input)
	print_string(strcmp_abc)
	print_space
	print_string(strcmp_ABCD)
	print_newline

	la $a0, strcmp_abc
	la $a1, strcmp_ABCD
	jal strcmp

	move $t0, $v0
	print_string(str_result)
	print_int($t0)
	print_newline

	print_string(str_input)
	print_string(strcmp_abc)
	print_space
	print_string(strcmp_abc)
	print_newline

	la $a0, strcmp_abc
	la $a1, strcmp_abc
	jal strcmp

	move $t0, $v0
	print_string(str_result)
	print_int($t0)
	print_newline

	print_string(str_input)
	print_string(str_abc)
	print_space
	print_string(strcmp_abc)
	print_newline

	la $a0, str_abc
	la $a1, strcmp_abc
	jal strcmp

	move $t0, $v0
	print_string(str_result)
	print_int($t0)
	print_newline

	#  TEST CASE for strncpy
	print_string(strncpy_header)
	print_string(str_input)
	print_string(str_helloworld)
	print_newline
	print_string(str_input)
	print_string(strncpy_buffer)
	print_newline
	
	la $a0, strncpy_buffer
	la $a1, str_helloworld
	li $a2, 49
	jal strncpy

	move $t0, $v0
	print_string(str_result)
	li $v0, PRINT_STRING
	move $a0, $t0
	syscall
	print_newline

	#  TEST CASE for indexof
	print_string(indexof_header)
	print_string(str_input)
	print_string(str_helloworld)
	print_newline

	la $a0, str_helloworld
	li $a1, 'o'
	jal indexOf

	move $t0, $v0
	print_string(str_result)
	print_int($t0)
	print_newline

	#  TEST CASE for reverse string
	print_string(reverse_header)
	print_string(str_input)
	print_string(str_helloworld)
	print_newline

	la $a0, str_helloworld
	jal reverse_str
	print_string(str_result)
	print_string(str_helloworld)
	print_newline

	#  TEST CASE for atoi
	print_string(atoi_header)
	print_string(str_input)
	print_string(atoi_str)
	print_newline

	la $a0, atoi_str
	jal atoi
	move $t0, $v0
	print_string(str_result)
	print_int($t0)
	print_newline
	print_string(str_input)
	print_string(atoi_12b5)
	print_newline

	la $a0, atoi_12b5
	jal atoi
	move $t0, $v0
	print_string(str_result)
	print_int($t0)
	print_newline

	# TEST CASE for  cut
	print_string(cut_header)
	print_string(str_input)
	print_string(cut_awesome)
	print_newline
	print_string(str_input)
	print_string(cut_pattern_start)
	print_newline
	print_string(str_input)
	print_string(cut_pattern_end)
	print_newline


	la $a0, cut_awesome
	la $a1, cut_pattern_start
	la $a2, cut_pattern_end
	jal cut

	move $t0, $v0
	print_string(str_result)
	move $a0, $t0
	li $v0, 34
	syscall
	print_newline

	print_string(str_result)
	print_int($v1)
	print_newline	
	
	# TEST CASE for strtok
	print_string(strtok_header)
	print_string(str_input)
	print_string(strtok_awesome)
	print_newline
	print_string(str_input)
	li $a0, ';'
	li $v0, 11
	syscall
	print_newline
			
	la $a0, strtok_awesome
main_loop:
 	li $a1 , ';'
	jal strtok

	# if strok returns NULL, QUIT program	
	beqz $v0, quit_main
	
	move $t0, $v0	
	print_string(str_result)
		
	#there is a token string, print its value	
	move $a0, $t0
	li $v0, PRINT_STRING
	syscall
	
	#set the argument to NULL and call strtok again
	move $a0, $0
	j main_loop
		
quit_main:
	#quit program
	li $v0, QUIT
	syscall



#################################################################
# Student defined functions will be included starting here
#################################################################

.include "hw2.asm"
