STRCMP - COMPLETE
-----------------
    1. Load the starting string addresses into registers.
    2. load the first byte into two separate register.
    3. loop through each byte and determine if they are equal.
        a. if they are not equal, return 0
    4. at any point, if a null terminator (\0) is hit, the string has ended, and we must check if the current characters are equal. if they are, then they are both null so we return 1. otherwise, return 0.

STRNCPY - COMPLETE
--------------
    1. Load the destination and source addresses into registers, along with the size of the destination.
    2. Copy the characters into the destination one by one. As you do this, one of three things will happen:
        a. The string will be longer than the destination buffer. In this case, once you've reached the end of the buffer, stop inserting elements and just insert a null terminator at the end of the string.
        b. The string will be smaller than the destrination buffer. In this case, simply add null terminators through the end of the string.
        c. The string will be precisely the same size as the buffer. What this really means is that it is one character smaller (because of the necessary null terminator character) so essentially this follows from situation (b).
    RETURN The address of the destination.

INDEXOF - COMPLETE
--------------
    Notes not needed for this one. It's pretty self explanatory.

REVERSE_STR - COMPLETE
------------------
    1. First find the start and end address of the string in question.
    2. Swap the first and last values and then move the address values closer together. This will result in then the second letter and the second to last letter being swapped. And so on and so forth.
    3. You are done when the end address < start address.

ATOI - COMPLETE
-----------
    The idea behind this one is pretty simple. Start with 0. Checking the characters one by one, if the character happens to be a number (which can be determined via math, not a switch statement) then multiply the current number by 10 and add the number in question. So If we currently have 12 and we come across a 7 in our traversal, we will do (12 * 10) + 7 = 127. And so on until you hit a non-numerical character. 

CUT - COMPLETE
----------
    This is probably the most complex function here. It will be easiest to break this function into phases. There are two phases, namely two things to verify before values (yes plural, remember that this function returns a tuple). The first thing to verify is that the source string contains the start pattern. If it does then it passes the first phase, so we store the start address of the start pattern. And now we can move on to the second phase. The second phase checks, from the end of the start pattern in the source string, if the end pattern is also contained in the string. If it is then we record the end address of the end pattern. Then we subtract the end address of the end by the start address of the start to get the string length. Then we return (start address of start pattern, pattern length).
    

STRTOK - COMPLETE
-------------
    This one was a little complex. Basically you want to step through the string until you find the delimiter. This part is easy as it's nearly identical to the indexof function. But the tricky part is handling the pointer to the next word. Basically, I use it like this:
    1. If we start at the start of a string, then it doesn't matter what's in the pointer; we will overwrite it.
    2. If we start at a token not at the start of a string, then we read the token's address from the pointer.
    3. If at any point in our traversal we hit a null terminator, that means we've hit the end of a string. When this happens we change the contents of the pointer to 0. This tells us, for the next time, to just return null.
