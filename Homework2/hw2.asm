##############################################################
# Homework #2
# name: Gustavo Poscidonio
# sbuid: 109 290 506
##############################################################

#Test modification

.text


strlen:
	#starting address of string in $a0
	add $t8, $a0, $0	#copy the address
	add $v0, $0, $0		#initialize count 
strlen_loop:
	lb $t0, 0($t8)		#load first char of string
	beqz $t0, end_strlen	#if NULL char, stop counting
	addi $v0, $v0, 1	# count++
	addi $t8, $t8, 1	# move address to next byte
	j strlen_loop
end_strlen:
	jr $ra


#EASY STRING FUNCTIONS

#STRING COMPARE
strcmp:
	#Define your code here 
	move $t8, $a0       #copy the address of the first string
	move $t9, $a1        #copy the address of the second string
    
strcmp_loop:
	lb $t0, 0($t8)      #load the first char of the first string
	lb $t1, 0($t9)      #load the first char of the second string
	bne $t0, $t1, strcmp_not_equal  #if the characters at the same iteration are not equal, the string is not equal

	#If either character is \0 then either both strings are at their end or they are not equal..
	beqz $t0, strcmp_final_check
	beqz $t1, strcmp_final_check

	#increment everything.
	addi $t8, $t8, 1
	addi $t9, $t9, 1

	j strcmp_loop

strcmp_final_check:
	#if the final characters in question are not equal, (meaning they are of different length) then they are not equal.
	bne $t0, $t1, strcmp_not_equal

strcmp_equal:
	#If the code reaches this point, then the strings must be equal.
	li $v0, 1
	j end_strcmp

strcmp_not_equal:
    	#Store 0 in the return value since the guys are not equal
    	li $v0, 0

end_strcmp:
	jr $ra
#END STRING COMPARE


#STRING COPY
strncpy:
	#Define your code here
    	move $t9, $a0   #Get the address of the destination string
    	move $t8, $a1   #Get the address of the source string
    	move $t7, $a2   #Get the size in bytes of the destination


strncpy_loop:
	lb $t0, 0($t8)      		#Place the source character into a temp register
	beqz $t0, strncpy_src_end	#Check if the character is a null character
	beqz $t7, strncpy_dst_end	#Check if we've used up all our space for our destination
	
	sb $t0, 0($t9)		#Here we copy the character
	addi $t8, $t8, 1	#Move to the next src char
	addi $t9, $t9, 1	#Move to the next dst char
	addi $t7, $t7, -1	#Decrement the number of spaces left.
	
	j strncpy_loop

strncpy_src_end:
	sb $zero, 0($t9)
	addi $t9, $t9, 1		#Move to the next dst char
	addi $t7, $t7, -1		#Decrement the number of spaces left.
	beqz $t7, strncpy_dst_end	#Check if we've used up all our space for our destination
	j strncpy_src_end
strncpy_dst_end:
	sb $zero, 0($t9)

strncpy_return:
    	move $v0, $a0
	jr $ra
#END STRING COPY



#INDEX OF
indexOf:
	#Define your code here
	
	move $t9, $a0		#Load the address of the src in a temp register
	move $t8, $a1	 	#Load the character to be searched for into a temporary register
	move $t0, $zero		#Init the counter to 0
	
indexOf_loop:
	lb $t1, 0($t9)		#Load the byte to be compared
	beq $t1, $t8, indexOf_return		#If the characters are equal, we've found what we're looking for
	beqz $t1, indexOf_end_src	#If we read a null terminator we have not found what we're looking for
	
	#Now increment
	addi $t0, $t0, 1
	addi $t9, $t9, 1
	
	j indexOf_loop
	
indexOf_end_src:
	li $t0, -1

indexOf_return:
	move $v0, $t0
	jr $ra
#END INDEX OF




#MEDIUM STRING FUNCTIONS

#REVERSE STRING
reverse_str:
	#Define your code here
	
	#Register values in this method
	# $t0 = start counter for the string
	# $t1 = end counter for the string
	# $t8 = start address for the string
	# $t9 = end address for the string
	# $t2 = temp register to hold one of the characters being swapped
	# $t3 = temp register to hold one of the characters being swapped
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	
	#Get the length of the string
	jal strlen
	
	#Init the start and end counters for the string
	move $t1, $v0
	move $t0, $zero
	
	#Get the start address of the string in question
	move $t8, $a0
	#Calculate the end address of the string
	add $t9, $t8, $t1
	addi $t9, $t9, -1
	
reverse_str_loop:
	#If the start and end pointers are equal or they cross, end the method
	beq $t0, $t1, reverse_str_end
	blt $t1, $t0, reverse_str_end
	
	#Swap the characters
	lb $t2, 0($t9)
	lb $t3, 0($t8)
	sb $t3, 0($t9)
	sb $t2, 0($t8)
	
	#Adjust the counters
	addi $t0, $t0, 1
	addi $t1, $t1, -1
	
	#Adjust the pointers
	addi $t8, $t8, 1
	addi $t9, $t9, -1
	
	j reverse_str_loop
	
reverse_str_end:
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	jr $ra
#END REVERSE STRING


#ASCII TO INTEGER
atoi:
	#Define your code here
	#In this function, $t9 will be a dummy place holder for instructions that need an immediate value but don't accept one.
	
	#The value to be returned (i.e. the output) will be stored in $t0. It starts at 0.
	move $t0, $zero
	
	#The pointer to the currect character will be in $t1
	move $t1, $a0
	
atoi_loop:
	

	#The character itself will be copied to $t2
	lb $t2, 0($t1)
	
	#The character encodings for numbers start at 48 so we subtract that to get the actual number in the bits
	addi $t2, $t2, -48
	
	#If we don't get a number between 0 and 9, then we've hit a bad character.
	bltz $t2, atoi_end
	li $t9, 9
	bgt $t2, $t9, atoi_end
	
	#Shift the output over by a factor of 10
	li $t9, 10
	mul $t0, $t0, $t9
	
	#Add the number in question to the output
	add $t0, $t0, $t2
	
	#Move on to the next character
	addi $t1, $t1, 1
	
	j atoi_loop

atoi_end:
	#return the output
	move $v0, $t0	
	jr $ra
#END ASCII TO INTEGER




#HARD STRING FUNCTIONS


#CUT STRING
cut:
	#Define your code here
	# The start of the string will be stored in $t0
	# The pointer that will move along the string will be stored in $t7
	# The pointer to the start pattern will be stored in $t1
	# The pointer used to iterate through the start pattern will be stored in $t8
	# The pointer to the end pattern will be stored in $t2
	# The pointer used to iterate through the end pattern will be stored in $t9
	# DUMMY variables will be stored in $t3 thru $t6. They will be used for comparisons
	
	#Init default return
	move $v0, $zero
	li $v1, -1
	
	#Init everything listed above
	move $t0, $a0
	move $t7, $a0
	move $t1, $a1
	move $t8, $a1
	move $t2, $a2
	move $t9, $a2
	
	
cut_check_start_loop:
	lb $t3, 0($t7)	#Load the character from the src string
	lb $t4, 0($t8)	#Load the character from the start pattern string
	beq $t3, $t4, cut_check_start_inner #If the first character matches, then jump to a loop to check the next character.
	beqz $t3, cut_end	# If the character we're checking is null, then we've reached the end of src and we have not found our character.
	addi $t7, $t7, 1	#Increment
	
	j cut_check_start_loop
	
cut_check_start_inner:
	#Here $t5 will store the original location of where we matched the string so we may jump back to it if necessary
	move $t5, $t7
	move $t6, $t8
	
cut_check_start_inner_loop:
	#Increment both pointers
	addi $t7, $t7, 1
	addi $t8, $t8, 1
	lb $t3, 0($t7)	#Load the character from the src string
	lb $t4, 0($t8)	#Load the character from the start pattern string

	beqz $t4, cut_check_end_loop	
	bne $t3, $t4, cut_return_to_outer_start_loop

	
	j cut_check_start_inner_loop
	
cut_return_to_outer_start_loop:
	move $t7, $t5
	addi $t7, $t7, 1
	move $t8, $t6
	j cut_check_start_loop	


cut_check_end_loop:
	lb $t3, 0($t7)	#Load the character from the src string
	lb $t4, 0($t9)	#Load the character from the end pattern string
	beq $t3, $t4, cut_check_end_inner #If the first character matches, then jump to a loop to check the next character.
	beqz $t3, cut_end # If the character we're checking is null, then we've reached the end of src and we have not found our pattern.
	addi $t7, $t7, 1
	
	j cut_check_end_loop
	
cut_check_end_inner:
	#Here $t6 will store the original location of where we matched the string so we may jump back to it if necessary
	move $t6, $t7
	move $t8, $t9
	
cut_check_end_inner_loop:
	#Increment both pointers
	addi $t7, $t7, 1
	addi $t9, $t9, 1
	lb $t3, 0($t7)	#Load the character from the src string
	lb $t4, 0($t9)	#Load the character from the start pattern string

	beqz $t4, PATTERN_FOUND	
	bne $t3, $t4, cut_return_to_outer_end_loop

	
	j cut_check_end_inner_loop

cut_return_to_outer_end_loop:
	move $t7, $t6
	addi $t7, $t7, 1
	move $t9, $t8
	j cut_check_end_loop	
	
PATTERN_FOUND:
	#We need to call strlen so save stuff onto the stack
	addi $sp, $sp, -16
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $s0, 8($sp)
	sw $s1, 12($sp)
	
	#move the things we need in to saved registers
	move $s0, $t5
	move $s1, $t6
	
	#load the end pattern into the string
	move $a0, $t2
	
	jal strlen
	
	#Move stuff back to where we had it so we don't break our code (this is bad form)
	move $t5, $s0
	move $t6, $s1
	
	#$t6 previously contained the start location of the end pattern but now we need the end location.
	add $t6, $t6, $v0
	
	#Load final values into return values
	move $v0, $t5
	sub $v1, $t6, $t5


	lw $s1, 12($sp)
	lw $s0, 8($sp)
	lw $a0, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 16
cut_end:
	jr $ra
#END CUT STRING



#STRING TOKENIZER
strtok:
	#Define your code here
	
	# The address of the string to be searched will be stored in $t0. This is also the address that will be returned.
	# The pointer that will step through the code will be located in $t1
	# The delimiter will be stored in $t2
	# The character being checked will be stored in $t3
	
	#Get the Delimiter
	move $t2, $a1
	
	#Check if the pointer is 0. This means use the same string.
	beqz $a0, strtok_input_addr_zero
	
	#If the pointer is not 0, just jump straight to the body.
	move $t0, $a0
	j strtok_body
	
strtok_input_addr_zero:
	#The input was null so we check the address of the pointer.
	lw $t0, ptr
	
	#If that pointer is 0, then we simply want to return nothing.
	beqz $t0, strtok_return_nothing
	
strtok_body:
	#Establish the pointer's starting location
	move $t1, $t0

strtok_loop:
	#Get the byte to be checked
	lb $t3, 0($t1)
	
	#End the function if we hit a '\0' terminator
	beqz $t3, strtok_delim_not_found
	
	#End the function if we find a delimiter
	beq $t3, $t2, strtok_delim_found
	
	#Increment our spot in the string
	addi $t1, $t1, 1
	
	j strtok_loop
	
	############## Remove these lines of code. These lines are only here to allow for main to continue working ###########
	#add $v0, $0, $0   #this makes strtok return a NULL
	###################################################################################################	

strtok_delim_found:
	#Insert '\0' character
	sb $zero, 0($t1)
	
	#Check for repeated delimiters
strtok_repeated_delims:
	#Increment by 1 to get start of next token
	addi $t1, $t1,1
	lb $t3, 0($t1)
	beq $t3, $t2, strtok_repeated_delims
	
	#Save the location of $t1 in ptr
	sw $t1, ptr
	
	#Return the location of the string
	move $v0, $t0
	
	#End the function
	j strtok_end
	
strtok_delim_not_found:
	#If we hit the end of the string, the last token. Store 0 in ptr so we know not to go through the string again.
	sw $zero, ptr
	move $v0, $t0
	j strtok_end
	
strtok_return_nothing:
	move $v0, $zero
	
strtok_end:
	jr $ra
#END STRING TOKENIZER
	


.data
	#Define your memory here
	.align 2
	ptr: .space 4
