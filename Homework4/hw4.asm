# Homework #4
# name: Gustavo Poscidonio
# sbuid: 109 290 506

.data
	dimension: .space 4
	inputBuffer: .space 60000 #LOL
	.align 2
	outputBuffer: .space 60000
	.align 2
	FRACTAL_BUF: .space 60000
	.data
	inputFileBuffer: .space 51
	outputFileBuffer: .space 51
	decode_message1_input: .asciiz "Enter the name of the file to use as input for decode_message1:\n"
	decode_message1_output: .asciiz "Enter the name of the file to use as output for decode_message1:\n"
	decode_message2_input: .asciiz "Enter the name of the file to use as input for decode_message2:\n"
	decode_message2_output: .asciiz "Enter the name of the file to use as output for decode_message2:\n"
	decode_message2_oops: .asciiz "\n\nApologies but I postponed this section of the assignment and was not able to complete it.\nIf you're interested, refer to my code for the basic logic that I was trying to complete before I ran out of time.\n\n"
	countN_message_arg1: .asciiz "Enter the number to be parsed for countN:\n"
	countN_message_arg2: .asciiz "Enter the single digit number to be searched for in countN:\n"
	boxFractal_message_degree: .asciiz "\nEnter the degree of a box fractal you would like to make:\n"
	boxFractal_message_char: .asciiz "Enter the character that represents a degree 0 fractal:\n"
	boxFractal_message_output: .asciiz "Enter the name of the output file for box fractal:\n"
	
.text

.macro remove_newlines(%label)
	la $t0, %label
	li $t2, '\n'
remove_newlines_loop:
	addi $t0, $t0, 1
	lb $t1, 0($t0)
	bne $t2, $t1, remove_newlines_loop
	li $t2, 0x0
	sb $t2, 0($t0)
.end_macro

.macro print_str(%label)
	li $v0, 4
	la $a0, %label
	syscall
.end_macro

.macro print_int(%int)
	li $v0, 1
	move $a0, %int
	syscall
.end_macro

.macro decode_message1_read_from(%inputAddress)
	li $v0, 14
	move $a0, %inputAddress
	la $a1, inputBuffer
	li $a2, 60000
	syscall
.end_macro

.macro pop_to_output(%output_pointer)
	pop_to_output_loop:
		beq $sp, $fp, pop_to_output_exit
		lw $t9, 0($sp)
		sb $t9, 0(%output_pointer)
		addi $sp, $sp, 4
		addi %output_pointer, %output_pointer, 1
		j pop_to_output_loop
	pop_to_output_exit:
.end_macro

.macro output_then_jump(%output_pointer, %jumb_label)
	pop_to_output(%output_pointer)
	
.end_macro

#########################################################################
### MAIN BEGIN
#########################################################################
main:

			################
		################################	
	##################################################
	############### DECODE MESSAGE 1 #################
	##################################################
		################################
			################
			
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(decode_message1_input)
	##################################################

	##################################################
	# Read the input file name
	li $v0, 8
	la $a0, inputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(decode_message1_output)
	##################################################
	
	##################################################
	# Read the output file name
	li $v0, 8
	la $a0, outputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# REMOVE NEWLINE CHARACTERS FROM FILE NAMES
	remove_newlines(inputFileBuffer)
	remove_newlines(outputFileBuffer)
	##################################################
	
	##################################################
	# OPEN INUPUT FILE FOR READING
	li $v0, 13 #syscall code for opening a file
	la $a0, inputFileBuffer #address of file name to open
	li $a1, 0 # 0 flag means read
	li $a2, 0 # ignore mode
	syscall
	move $s7, $v0 #Get the file descriptor
	##################################################
	
	##################################################
	# OPEN OUTPUT FILE FOR WRITING
	li $v0, 13 #syscall code for opening a file
	la $a0, outputFileBuffer #address of file name to open
	li $a1, 1 # 1 flag means write
	li $a2, 0 # ignore mode
	syscall	
	move $s6, $v0 #Get the file descriptor
	##################################################
	
	##################################################
	# DECODE MESSAGE 1
	move $a0, $s7
	move $a1, $s6
	jal decode_message1
	##################################################
	
	##################################################
	# BOX FRACTAL
	#li $a0, 4
	#li $a1, 'X'
	#move $a2, $s6
	#jal boxFractal
	##################################################
		
	##################################################
	# CLOSE INUPUT FILE
	li $v0, 16
	move $a0, $s7
	syscall
	##################################################
	
	##################################################
	# CLOSE OUTPUT FILE
	li $v0, 16
	move $a0, $s6
	syscall
	##################################################
	
			################
		################################	
	##################################################
	############### DECODE MESSAGE 2 #################
	##################################################
		################################
			################
			
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(decode_message2_input)
	##################################################

	##################################################
	# Read the input file name
	li $v0, 8
	la $a0, inputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(decode_message2_output)
	##################################################
	
	##################################################
	# Read the output file name
	li $v0, 8
	la $a0, outputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# DECLARE OOPS
	print_str(decode_message2_oops)
	##################################################
	
			################
		################################	
	##################################################
	#################### COUNT N #####################
	##################################################
		################################
			################
			
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(countN_message_arg1)
	##################################################
	
	##################################################
	# Read the first arg - we can just use the input 
	# file buffer for this
	li $v0, 8
	la $a0, inputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# REMOVE NEWLINE CHARACTERS FROM FILE NAMES
	remove_newlines(inputFileBuffer)
	
	##################################################
	# ATOI
	la $a0, inputFileBuffer
	jal atoi
	move $s0, $v0
	##################################################
	
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(countN_message_arg2)
	##################################################
	
	##################################################
	# Read the first arg - we can just use the input 
	# file buffer for this
	li $v0, 8
	la $a0, inputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# REMOVE NEWLINE CHARACTERS FROM FILE NAMES
	remove_newlines(inputFileBuffer)
	
	##################################################
	# ATOI
	la $a0, inputFileBuffer
	jal atoi
	move $s1, $v0
	##################################################
	
	##################################################
	# countN
	move $a0, $s0
	move $a1, $s1
	jal countN
	move $t0, $v0
	print_int($t0)
	##################################################
	
			################
		################################	
	##################################################
	################## BOX FRACTAL ###################
	##################################################
		################################
			################
			
	##################################################
	# PROMPT FOR DEGREE
	print_str(boxFractal_message_degree)
	##################################################
	
	##################################################
	# Read the first arg - we can just use the input 
	# file buffer for this
	li $v0, 8
	la $a0, inputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# REMOVE NEWLINE CHARACTERS FROM FILE NAMES
	remove_newlines(inputFileBuffer)
	
	##################################################
	# ATOI
	la $a0, inputFileBuffer
	jal atoi
	move $s0, $v0
	##################################################
	
	##################################################
	# PROMPT FOR DEGREE
	print_str(boxFractal_message_char)
	##################################################
	
	##################################################
	# Read the first arg - we can just use the input 
	# file buffer for this
	li $v0, 8
	la $a0, inputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# REMOVE NEWLINE CHARACTERS FROM FILE NAMES
	remove_newlines(inputFileBuffer)
	
	lb $s1, inputFileBuffer
	
			
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(boxFractal_message_output)
	##################################################
	
	##################################################
	# Read the output file name
	li $v0, 8
	la $a0, outputFileBuffer
	li $a1, 50
	syscall
	##################################################		
	
	##################################################
	# REMOVE NEWLINE CHARACTERS FROM FILE NAMES
	remove_newlines(outputFileBuffer)
	
	##################################################
	# OPEN OUTPUT FILE FOR WRITING
	li $v0, 13 #syscall code for opening a file
	la $a0, outputFileBuffer #address of file name to open
	li $a1, 1 # 1 flag means write
	li $a2, 0 # ignore mode
	syscall	
	move $s2, $v0 #Get the file descriptor
	##################################################
	
	##################################################
	# CALL THE FUNCTION
	move $a0, $s0
	move $a1, $s1
	move $a2, $s2
	
	jal boxFractal
	##################################################
	
	##################################################
	# EXIT THE PROGRAM
	li $v0, 10
	syscall
	##################################################
#########################################################################
### MAIN END
#########################################################################


#########################################################################
### DECODE MESSAGE 1 BEGIN
#########################################################################
decode_message1:
	#Save Everything
	addi $sp, $sp, -12
	sw $s7, 0($sp)
	sw $s6, 4($sp)
	sw $fp, 8($sp)
	
	#Save the descriptors in input:s6 output:s7
	move $s6, $a0
	move $s7, $a1
	
	la $t0, inputBuffer
	la $t1, outputBuffer
	
decode_message1_read:
	
	decode_message1_read_from($s6)
	beqz $v0, decode_message1_write_output
	
	
	
#Checking for open parens
	
decode_message1_loop1:
	lb $t2, 0($t0) #get the byte at the current place in input
	
	li $t3, '(' #load an open parents in for comparison
	beqz $t2, decode_message1_read #if we hit the end of our buffer, read more input.
	beq $t2, $t3, open_parens_detected #if open parens were detected, go into that block of code
	
	#$t5-8 will be used to store temp values we want to check against for invalid input
	slti $t5, $t2, 32 # set 1 if ascii < 48
	bnez $t5, decode_message1_write_output
	slti $t5, $t2, 33
	slti $t6, $t2, 48
	slt $t5, $t5, $t6
	bnez $t5, decode_message1_write_output
	slti $t5, $t2, 58
	slti $t6, $t2, 65
	slt $t5, $t5, $t6
	bnez $t5, decode_message1_write_output
	slti $t5, $t2, 91
	slti $t6, $t2, 97
	slt $t5, $t5, $t6
	bnez $t5, decode_message1_write_output
	slti $t5, $t2, 123
	beqz $t5, decode_message1_write_output
	
	
	addi $t0, $t0, 1 # input buffer pointer ++
	j decode_message1_loop1
	
open_parens_detected:
	addi $t0, $t0, 1 # i++ because we didnt hit it before.
	move $fp, $sp #save the frame pointer in case of invalid input.
	lb $t2, 0($t0) # get the current char
	addi $t1, $t1, 1 # move along in the output buffer
	li $t3, ')' #load a close parens for comparison. we can overwrite the open parens here because we don't need it anymore.
open_parens_detected_loop:
	beq $t2, $t3, close_parens_detected #Check if we hit a close parens
	
	#$t5-8 will be used to store temp values we want to check against for invalid input
	slti $t5, $t2, 32 # set 1 if ascii < 32
	bnez $t5, decode_message1_write_output
	slti $t5, $t2, 33
	slti $t6, $t2, 48
	slt $t5, $t5, $t6
	bnez $t5, decode_message1_write_output
	slti $t5, $t2, 58
	slti $t6, $t2, 65
	slt $t5, $t5, $t6
	bnez $t5, decode_message1_write_output
	slti $t5, $t2, 91
	slti $t6, $t2, 97
	slt $t5, $t5, $t6
	bnez $t5, decode_message1_write_output
	slti $t5, $t2, 123
	beqz $t5, decode_message1_write_output
	
	addi $sp, $sp, -4 #If we didn't hit anything invalid or a close parens we can move the stack pointer
	sw $t2, 0($sp) # in order to save what we want onto the stack
	addi $t0, $t0, 1 # move input buffer index along
	lb $t2, 0($t0) # get the next char
	j open_parens_detected_loop
	
close_parens_detected:
	addi $t0, $t0, 1 # move input buffer index along
	li $t4, '('
	sb $t4, 0($t1)
	addi $t1, $t1, 1
close_parens_detected_loop:
	beq $sp, $fp, close_parens_detected_loop_exit
	lw $t9, 0($sp)
	sb $t9, 0($t1)
	addi $sp, $sp, 4
	addi $t1, $t1, 1
	j close_parens_detected_loop
close_parens_detected_loop_exit:
	sb $t3, 0($t1)
	addi $t1, $t1, 1
	j decode_message1_loop1
	
decode_message1_write_output:
	li $v0, 15
	move $a0, $s7
	la $a1, outputBuffer
	li $a2, 255
	syscall
	
	#Put shit back
	lw $s7, 0($sp)
	lw $s6, 4($sp)
	lw $fp, 8($sp)
	addi $sp, $sp, 12

	jr $ra
#########################################################################
### DECODE MESSAGE 1 END
#########################################################################

#########################################################################
### DECODE MESSAGE 2 BEGIN
#########################################################################
decode_message2:
	#Save everything because why not
	addi $sp, $sp, -36
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	sw $ra, 32($sp)
	
	#Save the input file descriptor
	move $s6, $a0
	#As well as the output file descriptor
	move $s7, $a1
	
	#Get the addresses for the buffers
	la $t0, inputBuffer
	la $t1, outputBuffer

decode_message2_read:
	
	#Read the input
	decode_message1_read_from($s6)
	#If nothing was read, then the file is empty and we can leave.
	beqz $v0, decode_message2_write_output
	
	
decode_message2_loop1:
	# REMEMBER
	# $t0 - input buffer pointer
	# $t1 - output buffer pointer
	lb $t2, 0($t0) #get the byte at the current place in input
	
	beqz $t2, decode_message2_read #if we hit the end of our buffer, read more input.
	
	### CHECK FOR OPEN BRACES OF ALL KINDS
	li $t3, '(' #load an open parents in for comparison
	beq $t2, $t3, decode_message2_open_parens_detected #if open parens were detected, go into that block of code
	li $t3, '[' #load an open bracket for comparison
	beq $t2, $t3, decode_message2_open_bracket_detected #if open brackets were detected, go into that block of code
	li $t3 '{' #load an open curly for comparison
	beq $t2, $t3, decode_message2_open_curly_detected #if open curlies were detected, go into that block of code
	
	
	#$t5-8 will be used to store temp values we want to check against for invalid input
	slti $t5, $t2, 32 # set 1 if ascii < 48
	bnez $t5, decode_message2_write_output
	slti $t5, $t2, 33
	slti $t6, $t2, 48
	slt $t5, $t5, $t6
	bnez $t5, decode_message2_write_output
	slti $t5, $t2, 58
	slti $t6, $t2, 65
	slt $t5, $t5, $t6
	bnez $t5, decode_message2_write_output
	slti $t5, $t2, 91
	slti $t6, $t2, 97
	slt $t5, $t5, $t6
	bnez $t5, decode_message2_write_output
	slti $t5, $t2, 123
	beqz $t5, decode_message2_write_output
	
	
	addi $t0, $t0, 1 # input buffer pointer ++
	j decode_message2_loop1
	

decode_message2_open_parens_detected:
	addi $t0, $t0, 1 # i++ because we didnt hit it before.
	move $fp, $sp #save the frame pointer in case of invalid input.
	lb $t2, 0($t0) # get the current char
	addi $t1, $t1, 1 # move along in the output buffer
	li $t3, ')' #load a close parens for comparison. we can overwrite the open parens here because we don't need it anymore
	decode_message2_open_parens_detected_loop:
		beq $t2, $t3, decode_message2_close_parens_detected #Check if we hit a close parens
	
		#$t5-8 will be used to store temp values we want to check against for invalid input
		slti $t5, $t2, 32 # set 1 if ascii < 32
		bnez $t5, decode_message2_write_output
		slti $t5, $t2, 33
		slti $t6, $t2, 48
		slt $t5, $t5, $t6
		bnez $t5, decode_message2_write_output
		slti $t5, $t2, 58
		slti $t6, $t2, 65
		slt $t5, $t5, $t6
		bnez $t5, decode_message2_write_output
		slti $t5, $t2, 91
		slti $t6, $t2, 97
		slt $t5, $t5, $t6
		bnez $t5, decode_message2_write_output
		slti $t5, $t2, 123
		beqz $t5, decode_message2_write_output
		
		addi $sp, $sp, -4 #If we didn't hit anything invalid or a close parens we can move the stack pointer
		sw $t2, 0($sp) # in order to save what we want onto the stack
		addi $t0, $t0, 1 # move input buffer index along
		lb $t2, 0($t0) # get the next char
		j decode_message2_open_parens_detected_loop
		
	decode_message2_close_parens_detected:

decode_message2_open_bracket_detected:
	addi $t0, $t0, 1 # i++ because we didnt hit it before.
	move $fp, $sp #save the frame pointer in case of invalid input.
	lb $t2, 0($t0) # get the current char
	addi $t1, $t1, 1 # move along in the output buffer
	li $t3, ']' #load a close bracket for comparison. we can overwrite the open parens here because we don't need it anymore.

decode_message2_open_curly_detected:
	addi $t0, $t0, 1 # i++ because we didnt hit it before.
	move $fp, $sp #save the frame pointer in case of invalid input.
	lb $t2, 0($t0) # get the current char
	addi $t1, $t1, 1 # move along in the output buffer
	li $t3, '}' #load a close curly for comparison. we can overwrite the open parens here because we don't need it anymore.
	
	
decode_message2_write_output:
	li $v0, 15 #syscall for writing
	move $a0, $s7
	la $a1, outputBuffer
	li $a2, 60000
	syscall
	
	#Put shit back
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $s6, 24($sp)
	lw $s7, 28($sp)
	lw $ra, 32($sp)
	addi $sp, $sp, 36
	
	jr $ra
#########################################################################
### DECODE MESSAGE 2 END
#########################################################################


#########################################################################
### COUNT N 1 BEGIN
#########################################################################

countN:
	addi $sp, $sp, -8
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	move $s0, $zero
	beqz $a0, countN_base_case
	li $t0, 10 #use 10 to extract the last digit
	div $a0, $t0 #divide
	mfhi $t1
	beq $a1, $t1, countN_increment_return
	mflo $a0
	j countN_recurse

countN_increment_return:
	addi $s0, $s0, 1
	mflo $a0
	div $a0, $t0 #divide
	mfhi $t1
	beq $a1, $t1, countN_increment_return_again

countN_recurse:
	jal countN
	j countN_return

countN_increment_return_again:
	addi $s0, $s0, 1
	j countN_recurse
	
countN_base_case:
	move $v0, $zero
countN_return:
	add $v0, $v0, $s0
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	addi $sp, $sp, 8
	jr $ra

#########################################################################
### COUNT N 1 END
#########################################################################



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 



#########################################################################
### BOX FRACTAL BEGIN
#########################################################################

boxFractal:
	#SAVE EVERYTHING
	addi $sp, $sp, -20
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $ra, 16($sp)
	
	#now save all the args
	move $s0, $a0 #degree
	move $s1, $a1 #char to print
	move $s2, $a2 #output file descriptor
	
	#calculate the dimension of the matrix
	#the correct argument is already in there.
	jal calcMem
	
	#save it in s3
	move $s3, $v0 # DIMENSION
	sw $s3, dimension
	
	#load up the arguments to fill the matrix
	move $a0, $zero # row
	move $a1, $zero # column
	move $a2, $s0 # degree
	move $a3, $s3 # dimension
	la $t0, FRACTAL_BUF #get the address of the matrix buffer
	addi $sp, $sp, -8
	sw $t0, 0($sp) #address of fractal buf
	sw $s1, 4($sp) # char to print
	
	jal recursiveMatrixFill
	
	lw $t0, 0($sp)
	lw $s1, 4($sp)
	addi $sp, $sp, 8
	
	#get pointer for output buffer in $t9
	la $t9, outputBuffer
	
	#make sure that the value in $t0 is the base address of the matrix
	la $t0, FRACTAL_BUF
	
	#int row = 0
	move $t1, $zero
	
	#while(row != dimension)
boxFractalLoopRows:
	bgt $t1, $s3, boxFractalLoopRows_exit
	
	#int col = 0
	move $t2, $zero
	
	#while(cols != dimension)
	boxFractalLoopCols:
		beq $t2, $s3, boxFractalLoopCols_exit
		
		li $t5, '\0'
		lb $t6, 0($t0)
		beq $t5, $t6, null_encountered
		
		#if a null character was not encountered, write a character to the output buffer
		sb $s1, 0($t9)
		j boxFractalLoopCols_continue
		
		#if null, then print a space
		null_encountered:
		li $t5, ' '
		sb $t5, 0($t9)
		
		boxFractalLoopCols_continue:
		addi $t0, $t0, 1#increment matrix pointer
		addi $t9, $t9, 1#increment buffer pointer
		addi $t2, $t2, 1 # col++
		j boxFractalLoopCols
	
	boxFractalLoopCols_exit:
	addi $t1, $t1, 1 # row++
	li $t5, '\n'	#we reached the end of a row
	sb $t5, 0($t9)	#so we want to put a newline into the buffer
	addi $t9, $t9, 1#increment buffer pointer
	j boxFractalLoopRows
	
boxFractalLoopRows_exit:

	li $v0, 15 #syscall for write to file
	move $a0, $s2 #the file descriptor
	la $a1, outputBuffer
	mult $s3, $s3
	mflo $a2 #the number of bites to write is the square of the dimension
	add $a2, $a2, $s3
	syscall
	
	
	#PUT SHIT BACK
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $ra, 16($sp)
	addi $sp, $sp, 20
	jr $ra
	
calcMem:
	#Save Game
	addi $sp, $sp, -4
	sw $ra, 0($sp)

	li $t0, 1 # Check the base case
	beq $a0, $t0, calcMem_base_case
	
	#Not base case? then deduct 1 and call again
	addi $a0, $a0, -1
	jal calcMem
	
	#now multiply by 3 as per the formula
	li $t0, 3
	mult $v0, $t0
	
	#now return the low order 32 bit result
	mflo $v0
	
	#and return
	j calcMem_return
	
	
calcMem_base_case:
	li $v0, 1
	j calcMem_return
	
calcMem_return:
	#Load Game
	lw $ra, 0($sp)
	addi $sp, $sp 4
	jr $ra
	
recursiveMatrixFill:
	#The first four arguments are stored in $a0-$a3. The following two arguments will be assumed to be in 0($sp) and 4($sp)
	#As soon as the method starts however, we will put everything in saved registers
	addi $sp, $sp, -32
	sw $s0, 0($sp)
	sw $s1, 4($sp)
	sw $s2, 8($sp)
	sw $s3, 12($sp)
	sw $s4, 16($sp)
	sw $s5, 20($sp)
	sw $s6, 24($sp)
	sw $s7, 28($sp)
	
	#At this point, the arguments we want to save are in 32($sp) and 36($sp)
	move $s0, $a0 # int row
	move $s1, $a1 # int column
	move $s2, $a2 # int degree
	move $s3, $a3 # int dimension
	lw $s4, 32($sp) # char [][] matrix
	lw $s5, 36($sp) # char c
	
	#use $t0 for comparison here
	li $t0, 1
	beq $s2, $t0, recursiveMatrixFill_base_case
	
	#if the code reaches here, the case is not the base case. So GO GO GO RECURSION (to the theme of mighty morphin' power rangers)
	# $s7 will now hold the new degree to be passed down.
	addi $s7, $s2, -1
	# $s6 will now hold the new dimension
	li $t9, 3
	div $s3, $t9
	mflo $s6
	
	#########################################################################
	#	NOTE: 	For each recursion at this level, 			#
	#		a number of things remain constant.			#
	#		First off, the matrix address and 			#
	#		the character to be passed down				#
	#		never change. Furthermore, at this			#
	#		level, the degree and dimension that			#
	#		are being passed down are the same.			#
	#		So really, we can load all that stuff 			#
	#		in before any recursive calls then 			#
	#		just make changes to the rows and			#
	#		columns where appropriate.				#
	#########################################################################
	
	#Adjust the stack so all these methods know where to find their arguments.
	addi $sp, $sp, -12
	sw $s4, 0($sp) #store matrix for recursion
	sw $s5, 4($sp) #store char for recursion
	sw $ra, 8($sp) #because you have to
	
	#Now all we have to do is adjust the rows and columns which will involve using the new dimension in $t1
	
	
	
	# TOP LEFT RECURSION - no math to be done on rows and cols
	move $a0, $s0 #rows
	move $a1, $s1 #cols
	move $a2, $s7 #new degree for recursive calls
	move $a3, $s6 #new dimension for recursive calls
	
	jal recursiveMatrixFill
	
	# CENTER RECURSION - row + new_dim, col + new_dim ---- we do this first because we don't yet have to modifuy the new_dim
	add $a0, $s0, $s6 # row + new_dim
	add $a1, $s1, $s6 # col + new_dim
	move $a2, $s7 #new degree for recursive calls
	move $a3, $s6 #new dimension for recursive calls
	jal recursiveMatrixFill
	
	#The rest of these require a different new_dim, namely, new_dim * 2
	
	# TOP RIGHT RECURSION - row, col + (2 * new_dim)
	li $t0, 2	##
	mult $s6, $t0	###Multiplication
	mflo $t0	##
	move $a0, $s0 # row
	add $a1, $s1, $t0 # col + (2 * new_dim)
	move $a2, $s7 #new degree for recursive calls
	move $a3, $s6 #new dimension for recursive calls
	jal recursiveMatrixFill
	
	# BOTTOM LEFT RECURSION
	li $t0, 2	##
	mult $s6, $t0	###Multiplication
	mflo $t0	##
	add $a0, $s0, $t0 # row + (2 * new_dim)
	move $a1, $s1 # col
	move $a2, $s7 #new degree for recursive calls
	move $a3, $s6 #new dimension for recursive calls
	jal recursiveMatrixFill
	
	# BOTTOM RIGHT RECURSION
	li $t0, 2	##
	mult $s6, $t0	###Multiplication
	mflo $t0	##
	add $a0, $s0, $t0 # row + (2 * new_dim)
	add $a1, $s1, $t0 # col + (2 * new_dim)
	move $a2, $s7 #new degree for recursive calls
	move $a3, $s6 #new dimension for recursive calls
	jal recursiveMatrixFill
	
	#Put the stack back
	lw $s4, 0($sp) #store matrix for recursion
	lw $s5, 4($sp) #store char for recursion
	lw $ra, 8($sp) #because you have to
	addi $sp, $sp, 12
	
	j recursionMatrixFill_return
	
recursiveMatrixFill_base_case:
	#The computed address where the char should go will go in $t0
	move $t0, $s4 #get the matrix address
	lw $t9, dimension
	mult $s0, $t9 #multiply the row number by the dimension (this is a square matrix) to get the start index of that row
	mflo $t1 #put the result of the above in $t1. $t1 will now be designated to hold the offset from the baseaddress of the array
	add $t1, $t1, $s1 #add the column number to our offset
	#we don't have to multiply by the size of our data because our data is of size 1 (it's a char)
	add $t0, $t0, $t1 #add the base address to the offset
	sb $s5, 0($t0) #write the data in the array
	
recursionMatrixFill_return:
	lw $s0, 0($sp)
	lw $s1, 4($sp)
	lw $s2, 8($sp)
	lw $s3, 12($sp)
	lw $s4, 16($sp)
	lw $s5, 20($sp)
	lw $s6, 24($sp)
	lw $s7, 28($sp)
	addi $sp, $sp, 32
	jr $ra

#########################################################################
### BOX FRACTAL END
#########################################################################


#ASCII TO INTEGER
# Converts a string to an integer.
# @param $a0 address of string.
# @return $v0 Returns the integer value of the input string.
atoi:
	beqz $a0, atoi_done		# Check for NULL (0x0)
	move $t0, $a0
	li $v0, 0
	li $t9, 10
	li $t8, 0				# If this value is 1 then number is negative, else positive
	# Check for negative sign
	lb $t1, 0($t0)
	bne $t1, '-', atoi_loop
	# There was a negative sign so add 1 to pointer and mark this was negative
	addi $t0, $t0, 1
	li $t8, 1
atoi_loop:
	lb $t1, 0($t0)
	blt $t1, '0', atoi_sign				# Check to see if we have a valid character >= '0'
	bgt $t1, '9', atoi_sign				# Check to see if we have a valid characyer <= '9'
	# Subtract character zero from ASCII character
	addi $t1, $t1, -48
	# multiple sum * 10
	mult $v0, $t9
	mflo $v0
	# Add c - '0' to the sum
	add $v0, $v0, $t1
	# increment the address by 1
	addi $t0, $t0, 1
	j atoi_loop
atoi_sign:
	# Check to see if we need to negate the number
	bne $t8, 1, atoi_done
	# else we need to negate the final value
	not $v0, $v0			# Flip all the bits
	addi $v0, $v0, 1		# Add 1
atoi_done:
	jr $ra
#END ASCII TO INTEGER
