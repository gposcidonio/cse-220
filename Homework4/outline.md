## This outline is for part 2 of homework 4.

1. First, we have to iterate through the string.
    a. If we hit a valid alphanumeric character, then just keep moving. We haven't hit a secret message yet.
    b. If we hit an invalid char, return. Invalid chars include any non-alphanumeric characters except '(', '[', '{' and ' '
    c. If we hit an open bracket, write an open bracket to the output buffer and go to (2).

2. Here we are going through the string and now pushing things on to the stack. This is a loop.
    a. If we hit a valid alpha-numeric char or a space, then just push it on to the stack.
    b. If we hit an open bracket, pop the stack and write to the output buffer. Then write the new open bracket and restart the loop.
    c. If we hit a close bracket, check if it matches the current open bracket. If it does, then
    d. If we hit an invalid character, just reset the stack pointer to the frame pointer and write the output buffer and return.
