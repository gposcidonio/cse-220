 public class Fractal {
    
    public static void main(String args[])
    {
        //insert your testing code here to call boxFractal 
        //Note this call is different than the MIPS function your are creating. 
        //The MIPS function will additionally take a file descriptor for printing the fractal to the output file.
        boxFractal(7,'X');
    }

    /*
    * Calculates the Dimensions of the matrix recursively
    * Note this call is different than the MIPS function your are creating. 
    * The MIPS function will additionally take a file descriptor for printing the fractal to the output file.
    */
     public static void boxFractal(int var, char c){
        int dim;
        //calculte the dimension of the fractal
        dim = calcMem(var);
        char [][]matrix = new char [dim][dim]; 
        recursiveMatrixFill(0,0, var, dim, matrix, c);

        //Below traverses the two dimensional matrix and prints it character by character to the screen
        //Your code should be printing the array to the specified file
        for(int i = 0; i< dim; i++){
            for(int j = 0; j< dim; j++){
                if(matrix[i][j] == '\0'){
                    System.out.print(' ');
                }
                else{
                    System.out.print(matrix[i][j]);
                }
                
            }
            System.out.println();
        }
    }

    /*
    * Calculates the Dimensions of the matrix recursively
    */
    public static int calcMem(int n){
        if(n == 1){
            return 1;
        }
        else{
            return( 3 * calcMem(n - 1));
        }
    }
    
    /*
    * This is the recursive function that fills the two dimensional matrix
    */
    public static void recursiveMatrixFill(int row, int col, int degree, int dimension,char [][] matrix, char c){
        //Base case for degree 1, just print out one X
        if(degree == 1){ 
            // Print character c
            matrix[row][col] = c;
            
        }
        //Else this recursive call is called until the recursion becomes 1. 
        else{
            //Recursion pattern
            //(degree -1)           (degree -1)
            //           (degree -1)
            //(degree -1)           (degree -1)
            int r_d = dimension/3;
            recursiveMatrixFill(row, col, (degree-1), r_d , matrix, c);                             //Top left recrustion
            recursiveMatrixFill(row, (col + (2*r_d)), (degree-1), r_d, matrix, c);                  //Top right recursion
            recursiveMatrixFill((row + r_d), (col + (r_d)), (degree -1), r_d, matrix, c);           //Center recursion
            recursiveMatrixFill((row + (2*r_d)), col, (degree-1), r_d, matrix, c);                  //Bottom left recurstion
            recursiveMatrixFill((row + (2*r_d)), (col + (2*r_d)), (degree - 1), r_d, matrix, c);    //Bottom right recursion
        }
    }

}