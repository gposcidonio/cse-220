#
#
#
.data
	inputFileBuffer: .space 51
	outputFileBuffer: .space 51
	decode_message1_input: .asciiz "Enter the name of the file to use as input for decode_message1:\n"
	decode_message1_output: .asciiz "Enter the name of the file to use as output for decode_message1:\n"
	decode_message2_input: .asciiz "Enter the name of the file to use as input for decode_message2:\n"
	decode_message2_output: .asciiz "Enter the name of the file to use as output for decode_message2:\n"

.text

.macro remove_newlines(%label)
	la $t0, %label
	li $t2, '\n'
remove_newlines_loop:
	addi $t0, $t0, 1
	lb $t1, 0($t0)
	bne $t2, $t1, remove_newlines_loop
	li $t2, 0x0
	sb $t2, 0($t0)
.end_macro

.macro print_str(%label)
	li $v0, 4
	la $a0, %label
	syscall
.end_macro

main:
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(decode_message1_input)
	##################################################

	##################################################
	# Read the input file name
	li $v0, 8
	la $a0, inputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# PROMPT FOR INPUT FILE NAME
	print_str(decode_message1_output)
	##################################################
	
	##################################################
	# Read the output file name
	li $v0, 8
	la $a0, outputFileBuffer
	li $a1, 50
	syscall
	##################################################
	
	##################################################
	# REMOVE NEWLINE CHARACTERS FROM FILE NAMES
	remove_newlines(inputFileBuffer)
	remove_newlines(outputFileBuffer)
	##################################################
	
	##################################################
	# OPEN INUPUT FILE FOR READING
	li $v0, 13 #syscall code for opening a file
	la $a0, inputFileBuffer #address of file name to open
	li $a1, 0 # 0 flag means read
	li $a2, 0 # ignore mode
	syscall
	move $s7, $v0 #Get the file descriptor
	##################################################
	
	##################################################
	# OPEN OUTPUT FILE FOR WRITING
	li $v0, 13 #syscall code for opening a file
	la $a0, outputFileBuffer #address of file name to open
	li $a1, 1 # 1 flag means write
	li $a2, 0 # ignore mode
	syscall	
	move $s6, $v0 #Get the file descriptor
	##################################################
	
	##################################################
	# DECODE MESSAGE 1
	#move $a0, $s7
	#move $a1, $s6
	#jal decode_message1
	##################################################
	
	##################################################
	# BOX FRACTAL
	li $a0, 4
	li $a1, 'X'
	move $a2, $s6
	jal boxFractal
	##################################################
		
	##################################################
	# CLOSE INUPUT FILE
	li $v0, 16
	move $a0, $s7
	syscall
	##################################################
	
	##################################################
	# CLOSE OUTPUT FILE
	li $v0, 16
	move $a0, $s6
	syscall
	##################################################
	
	##################################################
	# EXIT THE PROGRAM
	li $v0, 10
	syscall
	##################################################
	
	
	

.include "hw4.asm"
