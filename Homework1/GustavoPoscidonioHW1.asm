# Homework 1
# name: Gustavo Poscidonio
# sbu id: 109 290 506

#
#		NOTE
#	- @s complement will be stored in t0
#	- sign mag will be stored in t1
#

##############
###############
################
####  DATA  ####
################
###############
##############
.data
	enter_int: .asciiz "Enter an integer number: "
	twos_comp: .asciiz "2's complement: \t"
	sign_mag: .asciiz "Sign Magnitude: \t"
	neg_twos_comp: .asciiz "Neg 2's complement: \t"
	ieee: .asciiz "IEEE-754 single precision: \t"
	little_end: .asciiz "Little Endian: \t"
	big_end: .asciiz "Big Endian: \t"
	space: .asciiz " "
	times_two_to_the: " * 2^"
	endl: .asciiz "\n"
	tab: .asciiz "\t"
	plus: .asciiz "+ 1."
	minus: .asciiz "- 1."
	
################
################
################
####  TEXT  ####
################
################
################
.text


##################
####  MACROS  ####
##################

#Print string from .data macro
.macro printStr (%string)
	la $a0, %string
	li $v0, 4
	syscall
.end_macro

#Print number in register %num as interpreted as a twos complement number
.macro printTwosComp (%num)
	move $a0, %num
	li $v0, 1
	syscall
.end_macro

#Print number in register %num as a hexadecimal
.macro printHex (%num)
	move $a0, %num
	li $v0, 34
	syscall
.end_macro

#Print number in register %num as a binary
.macro printBinary (%num)
	move $a0, %num
	li $v0, 35
	syscall
.end_macro

#Print number in register %num as a interpreted as a signed magnitude numbe
.macro printSM (%num)
	move $a0, %num
	li $v0, 101
	syscall
.end_macro

#Converts to Sign Magnitude and places that value in the second argument
.macro convertToSM(%to, %from)
	move %to, %from
	bltz %to, if_neg
	j else
if_neg:
	#First we negate the two's complement number to get the unsigned number
	
	#This bit of code simulates the "neg" functionality but it prevents overflow
	#if the largest negative number ( -2147483648) is used as input.
	not %to, %to		#Flip the bits
	addiu %to,%to, 1	#Add 1 as required by the negation of two's complement
	
	lui $t9, 0x8000 	#Now we load the number 1000 0000 0000 0000 into the upper 16 bits of $t9
	or %to, %to, $t9 	#We did the previous line in order to "or" it with the
				#target value. This changes the MSB to 1 making the value negative.
				
else: #do nothing since if it's positive, we don't change it.
.end_macro

#Determines the sign of the value at the given address and prints either "+" or "-"
.macro printSign(%num)
	lui $t9, 0x8000
	and $t9, %num, $t9
	beqz $t9,if_zero
	j else
if_zero:
	printStr(plus)
	j finally
else:
	printStr(minus)
finally: #Placeholder so if_zero can skip the else isntructions
.end_macro

#Place the exponent of the IEEE format number into %to
.macro extract_exponent(%to, %from)
	sll %to, %from, 1
	srl %to, %to, 24
	addi %to, %to, -127
.end_macro

#Place the fraction of the IEEE format number into %to
.macro extract_fraction(%to, %from)
	sll %to, %from, 9
.end_macro

#This method extracts groups of 8 bits and places them in the lower 8 bits of $t6-$t9
#For a binary number denoted by AAAAAAAABBBBBBBBCCCCCCCCDDDDDDDD
#	$t6 = 000000000000000000000000AAAAAAAA
#	$t7 = 000000000000000000000000BBBBBBBB
#	$t8 = 000000000000000000000000CCCCCCCC
#	$t9 = 000000000000000000000000DDDDDDDD
# NOTE: In the comment above, bits that share the same letter as a place holder
# do no necessarily share the same value.
.macro load_big_endian_into_t6_thru_t9(%num)
	srl $t6, %num, 24 	#Load firt 8 bits into $t6
	
	sll $t7, %num, 8	#Delete the upper 8 bits using a shift
	srl $t7, $t7, 24	#Load the second set of 8 bits
	
	sll $t8, %num, 16	#Delete the upper 16 bits using a shift
	srl $t8, $t8, 24	#Load the third set of 8 bits
	
	sll $t9, %num, 24	#Delete the upper 24 bits using a shift
	srl $t9, $t9, 24	#Load the fourth set of 8 bits	
.end_macro

#Prints the bits that are in $t6-$t9 in big endian format
#along with each bit's ascii representation
.macro print_t6_thru_t9_big

	move $a0, $t6 	#Load contents of register $t6 into argument register.
	li $v0, 1	#syscall code for printing an integer
	syscall
	li $v0 11	#syscall code for printing a character
	syscall
	
	printStr(tab)	#Formatting
	
	move $a0, $t7	#Same as code above except now for $t7
	li $v0, 1
	syscall
	li $v0 11
	syscall
	
	printStr(tab)
	
	move $a0, $t8	#Same as code above except now for $t8
	li $v0, 1
	syscall
	li $v0 11
	syscall
	
	printStr(tab)
	
	move $a0, $t9	#Same as code above except now for $t9
	li $v0, 1
	syscall
	li $v0 11
	syscall
.end_macro

#Prints the bits that are in $t6-$t9 in little endian format
#along with each bit's ascii representation
.macro print_t6_thru_t9_little

	move $a0, $t9 	#Load contents of register $t9 into argument register.
	li $v0, 1	#syscall code for printing an integer
	syscall
	li $v0 11	#syscall code for printing a character
	syscall
	
	printStr(tab)	#Formatting
	
	move $a0, $t8	#Same as code above except now for $t8
	li $v0, 1
	syscall
	li $v0 11
	syscall
	
	printStr(tab)
	
	move $a0, $t7	#Same as code above except now for $t7
	li $v0, 1
	syscall
	li $v0 11
	syscall
	
	printStr(tab)
	
	move $a0, $t6	#Same as code above except now for $t6
	li $v0, 1
	syscall
	li $v0 11
	syscall
.end_macro



.globl _main

_main:
	
	###############################
	#       Take User Input       #
	###############################
	printStr(enter_int)	#Request user input
	jal _read_int		#Receive user input
	printStr(endl)		#Formatting
	printStr(endl)		#Formatting
	
	###############################
	#       Print Twos Comp       #
	###############################
	printStr(twos_comp)	#Title for twos comp section
	printTwosComp($t0)	#REP_VALUE
	printStr(tab)
	printHex($t0)		#HEX_VALUE
	printStr(tab)
	printBinary($t0)	#BINARY_VALUE
	printStr(tab)
	printTwosComp($t0)	#TWOS_COMP
	printStr(endl)
	
	####################################
	#       Print Sign Magnitude       #
	####################################
	printStr(sign_mag)	#Title for twos comp section
	convertToSM($t1,$t0)
	printSM($t1)		#REP_VALUE
	printStr(tab)
	printHex($t1)		#HEX_VALUE
	printStr(tab)
	printBinary($t1)	#BINARY_VALUE
	printStr(tab)
	printTwosComp($t1)	#TWOS_COMP
	printStr(endl)
	
	####################################
	#       Print Neg Twos Comp        #
	####################################
	printStr(neg_twos_comp)	#Title for twos comp section
	
	#This bit of code simulates the "neg" functionality but it prevents overflow
	#if the largest negative number ( -2147483648) is used as input.
	not $t2, $t0		#Flip the bits
	addiu $t2, $t2, 1	#Add 1 as required by the negation of two's complement
	
	printTwosComp($t2)	#REP_VALUE
	printStr(tab)
	printHex($t2)		#HEX_VALUE
	printStr(tab)
	printBinary($t2)	#BINARY_VALUE
	printStr(tab)
	printTwosComp($t2)	#TWOS_COMP
	printStr(endl)
	
	####################################
	#          Print IEEE-754          #
	####################################
	printStr(ieee)			#Title for twos comp section
	move $t3, $t0			#Move the original number into $t3 for computation
	printSign($t3)			#Find the sign of the number based on the MSB and print it
	extract_exponent($t4,$t3)	#Get the exponent of the IEEE number
	extract_fraction($t5,$t3)	#Get the fraction of the IEEE number
	printBinary($t5)		#Print the fraction
	printStr(times_two_to_the)	#Print "* 2^"
	printTwosComp($t4)		#Print the exponent
	printStr(endl)
	
	####################################
	#   Split Bits for Endian Reading  #
	####################################
	load_big_endian_into_t6_thru_t9($t0) 	#This macro takes the bits in $t0 and loads them sequentially
						#in groupd of four. See the comment before the macro for more info.
	
	####################################
	#          Little Endian           #
	####################################
	printStr(little_end)		#Print the string "Little Endian: "
	print_t6_thru_t9_little		#Print the registers $t6-$t9 that were generated above in little endian order.
	printStr(endl)			#Formatting
	
	####################################
	#             Big Endian           #
	####################################
	printStr(big_end)		#Print the string "Big Endian: "
	print_t6_thru_t9_big		#Print the registers $t6-$t9 that were generated above in big endian order.
	
	j _exit
	
	
	
#This function will read an integer and move it into $t0
_read_int:
	li $v0, 5
	syscall
	#The syscall will move the value into $v0 so move it to $t0
	move $t0, $v0
	jr $ra

#Readable exit function
_exit:
	li $v0, 10
	syscall
	
