##############################################################
# Homework #3
# name: Gustavo Poscidonio
# sbuid: 109 290 506
##############################################################
#
#	EXTRA CREDIT COMPLETED: ***LOGO ONLY***
#	SCROLLING HAS NOT BEEN IMPLEMENTED
#
##############################################################
# P.S.
#	So this code can get ugly and downright lazy
#	sometimes, so sorry about that.
#
##############################################################
.data

	.eqv DEFAULT_COLOR 0x0F

	cursor: .word 0xFFFF0000
	ptr: .space 4
	current_color: .byte 0x0F
	temp_color: .byte 0x00
	buffer: .space 256
	begin_escape: .asciiz "\\033["
	end_escape: .asciiz "m"
	ascii_art:"\\n\\n\\n\\n\\n\\n\\n\\033[0;40;1;33m               _____        _____        _____                             \\n\\033[0;40;1;32m            __|___  |__  __|___  |__  __|___  |__   ______  ______  _____  \\n\\033[0;40;1;36m           |   ___|    ||   ___|    ||   ___|    | |____  ||____  |/     | \\n\\033[0;40;1;34m           |   |__     | `-.`-.     ||   ___|    | |    --||    --||  /  | \\n\\033[0;40;1;35m           |______|  __||______|  __||______|  __| |______||______||_____/ \\n\\033[0;40;1;31m              |_____|      |_____|      |_____|                            \\n                                                                "
	

.text

.macro getColor(%bold,%color)

.end_macro

printk:

	# $s7 will hold the current color
	# $s0 will point to the current location in the string argument
	
	#We will be using lots of functions here so let's just save all the $s registers
	#In case we need them. We can go back and change things before we're done.
	addi $sp, $sp, -36
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	
	#Get the string out of the argument register
	move $s0, $a0
	
	
	
printk_loop:
	#get the char
	lb $s1, 0($s0)
	
	#check for null terminator
	beqz $s1, printk_end
	
	#check for escape sequence
	li $t0, '\\'
	beq $s1, $t0, escape_detected_true
	j continue_after_escape_detected
	escape_detected_true:
		move $a0, $s0
		jal escape_detected
		#Adjust the pointer
		addi $s0, $s0, 1
		#skip some number of characters
		add $s0, $s0, $v0
		j printk_loop
	continue_after_escape_detected:

	#Get the current color
	lb $s7, current_color
	
	#load args for putck
	move $a0, $s1
	move $a1, $s7
	
	jal putck
	
	
	#Adjust the pointer
	addi $s0, $s0, 1
	
	j printk_loop
	
	
	
	
printk_end:
	
	#Put shit back
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	addi $sp, $sp, 36
	jr $ra
	
putk:
	#Load the default color
	li $a1, DEFAULT_COLOR
	
	#Manage stack and call putck using the current char and the default color above.
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	
	jal putck
	
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	jr $ra
	
putck:
	#Copy the data to be written.
	move $t0, $a0 #Character
	move $t1, $a1 #Color
	
	#Calculate halfword to be stored.
	sll $t1, $t1, 8
	add $t0, $t0, $t1
	
	#Write (print) the data
	lw $t3, cursor
	sh $t0, 0($t3)
	
	#Move the cursor
	addi $t3, $t3, 2
	sw $t3, cursor
	
	
	jr $ra

putck_xy:
	#We will be using lots of functions here so let's just save all the $s registers
	#In case we need them. We can go back and change things before we're done.
	addi $sp, $sp, -36
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	
	#Save the cursor location
	lw $s0, cursor
	
	#Get the x and y coords
	move $s1, $a1
	move $s2, $a2
	#calculate new cursor location
	li $t0, 160
	mult $s2, $t0
	mflo $t1
	add $t2, $s1, $s1
	add $t1, $t1, $t2
	addi $t1, $t1, 0xFFFF0000
	#now store that in the cursor address
	sw $t1, cursor
	
	
	jal putck
	
	#put the cursor back
	sw $s0, cursor
	
	#Put shit back
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	addi $sp, $sp, 36
	
	jr $ra
	
putk_xy:
	#We will be using lots of functions here so let's just save all the $s registers
	#In case we need them. We can go back and change things before we're done.
	addi $sp, $sp, -36
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	
	#Save the cursor location
	lw $s0, cursor
	
	#Get the x and y coords
	move $s1, $a1
	move $s2, $a2
	#calculate new cursor location
	li $t0, 160
	mult $s2, $t0
	mflo $t1
	add $t2, $s1, $s1
	add $t1, $t1, $t2
	addi $t1, $t1, 0xFFFF0000
	#now store that in the cursor address
	sw $t1, cursor
	
	
	jal putk
	
	#put the cursor back
	sw $s0, cursor
	
	#Put shit back
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	addi $sp, $sp, 36
	
	jr $ra
	
printk_xy:
	#We will be using lots of functions here so let's just save all the $s registers
	#In case we need them. We can go back and change things before we're done.
	addi $sp, $sp, -36
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	
	#Save the cursor location
	lw $s0, cursor
	
	#Get the x and y coords
	move $s1, $a1
	move $s2, $a2
	#calculate new cursor location
	li $t0, 160
	mult $s2, $t0
	mflo $t1
	add $t2, $s1, $s1
	add $t1, $t1, $t2
	addi $t1, $t1, 0xFFFF0000
	#now store that in the cursor address
	sw $t1, cursor
	
	jal printk 
	
	#put the cursor back
	sw $s0, cursor
	
	#Put shit back
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	addi $sp, $sp, 36
	
	jr $ra
		
clear:
	#Start address of MMIO
	li $t0, 0xFFFF0000
	li $t1, 0xFFFF0FA0
	#Establish a temp cursor
	move $t2, $t0
	
clear_loop:
	#clear the char
	sb $zero, 0($t2)
	#move the cursor
	addi $t2, $t2, 1
	#Check if we're at the end
	beq $t1, $t2, clear_end
	
	j clear_loop
	
clear_end:
	#House keepiing
	#move cursor to init position
	sw $t0, cursor
	
	jr $ra
	
ec:
	#We will be using lots of functions here so let's just save all the $s registers
	#In case we need them. We can go back and change things before we're done.
	addi $sp, $sp, -36
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	
	jal clear
	
	la $a0, ascii_art
	
	jal printk
	
	#Put shit back
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	addi $sp, $sp, 36
	jr $ra
	

escape_detected:
	#We will be using lots of functions here so let's just save all the $s registers
	#In case we need them. We can go back and change things before we're done.
	addi $sp, $sp, -36
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	
	#Grab the address
	move $s0, $a0
	
	#Check for ANSI
	la $a1, begin_escape
	la $a2, end_escape
	
	jal cut
	
	beq $s0, $v0, escape_ansi
	
	#Increment the address by one
	addi $s0, $s0, 1
	
	lb $s1, 0($s0)
	
	#Check Cases
	li $t0, 'n'
	beq $s1, $t0, escape_new_line
	li $t0, 't'
	beq $s1, $t0, escape_tab
	li $t0, 'b'
	beq $s1, $t0, escape_backspace
	
	#If we reach here, there are no other possible escapes so the escape character must be invalid.
	j escape_invalid
	
	
	escape_new_line:
		#Get current cursor location
		lw $t1, cursor
		sll $t1, $t1, 16
		srl $t1, $t1, 16
		#Extract its position in the x axis
		li $t2, 160
		div $t1, $t2 #LO - Quotient #HI - Remainder
		mfhi $t3
		#Compute how many spaces to add to cursor
		sub $t2, $t2, $t3
		#Add those spaces
		add $t1, $t1, $t2
		#add $t1, $t1, $t1
		addi $t1, $t1, 0xFFFF0000
		#Store the new address
		sw $t1, cursor
		#return the number of characters to skip
		li $v0, 1
		j escape_end
		
	escape_tab:
		li $a0, 32
		lb $a1, current_color
		jal putck
		jal putck
		jal putck
		jal putck
		jal putck
		
		#return the number of characters to skip
		li $v0, 1
		j escape_end
		
	escape_backspace:
		lw $t0, cursor
		addi $t0, $t0, -2
		sw $t0, cursor
		
		#return the number of characters to skip
		li $v0, 1
		j escape_end
	
	escape_ansi:
	#$s0 here contains the start address of the ansi code
		#Get the length of the ansi string
		move $s1, $v1
		#This same length will also be the return value
		move $s7, $v1
		#Get the address of the buffer
		la $t9, buffer
		escape_ansi_loop:
			#pull the char from src and push to buffer
			lb $t0, 0($s0)
			sb $t0, 0($t9)
			#adjust the buffer pointer and the src pointer by 1
			addi $s0, $s0, 1
			addi $t9, $t9, 1
			#Decrement size
			addi $s1, $s1, -1
			
			beqz $s1, break_escape_ansi_loop
			
			j escape_ansi_loop
			
		break_escape_ansi_loop:
			sb $zero, 0($t9)
			la $a0, buffer
			
			jal parse_escape_code
			
			sb $v0, current_color
			
			addi $s7, $s7, -1
			move $v0, $s7
		
		j escape_end
	escape_invalid:
		li $v0, 1
escape_end:
	#Put shit back
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	addi $sp, $sp, 36
	
	jr $ra
	
#PARSING ESCAPE CODE
parse_escape_code:
	#We will be using lots of functions here so let's just save all the $s registers
	#In case we need them. We can go back and change things before we're done.
	addi $sp, $sp, -36
	sw $ra, 0($sp)
	sw $s0, 4($sp)
	sw $s1, 8($sp)
	sw $s2, 12($sp)
	sw $s3, 16($sp)
	sw $s4, 20($sp)
	sw $s5, 24($sp)
	sw $s6, 28($sp)
	sw $s7, 32($sp)
	
	#First, hold on the the address of the string in case it gets overwritten 
	move $s0, $a0
	
	#Now determine if the string is valid by checking the length against the cut length
	#The string in question should already be in the argument register.
	#First the length
	jal strlen
	#Save the result
	move $s7, $v0
	#Now we cut
	la $a1, begin_escape
	la $a2, end_escape
	jal cut
	#Save the cut result
	move $s6, $v1
	#If the escape sequence length is different from the overall length, then it must
	#be the case that the sequence is invalid.
	bne $s6, $s7, parse_invalid_input
	
	#Check if it's an empty escape code
	li $t0, 6
	beq $s6, $t0, parse_default_output #Not actually invalid but output should be same as invalid input.
	
	#Now we know the the end of the string is valid so let's change it to a null terminator to make
	#Our loop easier to manage.
	add $t0, $s0, $s7
	li $t1, 0x0
	sb $t1, 0($t0)
	
	#Okay now move the cursor past the beginning escape sequence
	addi $s0, $s0, 5
	
	#Now we prep for our loop by loading the address of our string the firs time around for strtok.
	#Subsequent calls will load 0x0.
	move $a0, $s0
	li $a1, ';'
	
	#Now we have to tokenize our string based on the delimiter initially listed above.
	jal strtok
	
parsing_loop:
	#In this loop, we shall observe that $s1 should hold our foreground color and $s2 should hold our background color.
	#$s3 will hold whether or not the color is bold.
	
	#If the address returned from the tokenizer is 0, there are no more tokens to be processed.
	beqz $v0, break_parsing_loop
	
	#Now we process what was returned into a number so we can check its validity
	move $a0, $v0
	
	jal atoi
	
	#The value currently being processed will be held in $s4
	move $s4, $v0
	
	#Is our color bold?
	beqz $s4, parsing_bold_false
	li $t1, 1
	beq $s4, $t1, parsing_bold_true
	
	#Now we check the validity of the numer. It may be in the range [30,37]U[39,47]U[49]
	li $t0, 30
	blt $s4, $t0, parse_invalid_input # x < 30
	li $t0, 38
	beq $s4, $t0, parse_invalid_input # x = 38
	li $t0, 48
	beq $s4, $t0, parse_invalid_input # x = 48
	li $t0, 49
	bgt $s4, $t0, parse_invalid_input # x > 49
	
	#Now extract the 10s and the 1s digits
	li $t0, 10
	div $s4, $t0
	mfhi $t1 #remainder
	mflo $t2 #quotient
	
	#Check if the color belongs to the foreground or background and branch appropriately.
	li $t0, 3
	beq $t2, $t0, parsing_foreground_color_found
	li $t0, 4
	beq $t2, $t0, parsing_background_color_found
	
parsing_foreground_color_found:
	#If the bold bit is negative or zero then it should be zero, otherwise it should be one
	slt $s3, $zero, $s3
	#Get the foreground color
	move $s1, $t1
	#Shif the bold bit for adding
	sll $t1, $s3, 3
	#add the two guys to get the complete 4 bit color code.
	add $s1, $s1, $t1
	#Let the next token know that the bold bit was used
	addi $s3, $s3, -100

	j continue_parsing_loop
	
parsing_background_color_found:
	#If the bold bit is negative or zero then it should be zero, otherwise it should be one
	slt $s3, $zero, $s3
	#Get the background color
	move $s2, $t1
	#Shif the bold bit for adding
	sll $t1, $s3, 3
	#add the two guys to get the complete 4 bit color code.
	add $s2, $s2, $t1
	#Let the next token know that the bold bit was used
	addi $s3, $s3, -100
	
	j continue_parsing_loop
	
parsing_bold_true:
	bltz $s3, parsing_dont_load_default_true
	li $v0, DEFAULT_COLOR
	
	parsing_dont_load_default_true:
		move $s3, $zero
		addi $s3, $s3, 1
		j continue_parsing_loop
	
parsing_bold_false:
	bltz $s3, parsing_dont_load_default_false
	li $v0, DEFAULT_COLOR
	
	parsing_dont_load_default_false:
		move $s3, $zero
		j continue_parsing_loop
	

	
	

continue_parsing_loop:
	li $a0, 0x0
	li $a1, ';'
	
	jal strtok
	
	j parsing_loop

break_parsing_loop:

	sll $s2, $s2, 4
	add $v0, $s1, $s2
	
	bge $s3, $zero, parse_default_output
	li $v1, 0
	
parsing_put_stuff_back:	
	#Put shit back
	lw $ra, 0($sp)
	lw $s0, 4($sp)
	lw $s1, 8($sp)
	lw $s2, 12($sp)
	lw $s3, 16($sp)
	lw $s4, 20($sp)
	lw $s5, 24($sp)
	lw $s6, 28($sp)
	lw $s7, 32($sp)
	addi $sp, $sp, 36
	
	j parse_end
	
parse_invalid_input:
	li $v0, DEFAULT_COLOR
	li $v1, 1 #error
	j parsing_put_stuff_back
	
parse_default_output:
	li $v0, DEFAULT_COLOR
	li $v1, 0 #ok just default
	j parsing_put_stuff_back
	
parse_end:
	jr $ra
#END PARSING ESCAPE CODE

#ASCII TO INTEGER
# Converts a string to an integer.
# @param $a0 address of string.
# @return $v0 Returns the integer value of the input string.
atoi:
	beqz $a0, atoi_done		# Check for NULL (0x0)
	move $t0, $a0
	li $v0, 0
	li $t9, 10
	li $t8, 0				# If this value is 1 then number is negative, else positive
	# Check for negative sign
	lb $t1, 0($t0)
	bne $t1, '-', atoi_loop
	# There was a negative sign so add 1 to pointer and mark this was negative
	addi $t0, $t0, 1
	li $t8, 1
atoi_loop:
	lb $t1, 0($t0)
	blt $t1, '0', atoi_sign				# Check to see if we have a valid character >= '0'
	bgt $t1, '9', atoi_sign				# Check to see if we have a valid characyer <= '9'
	# Subtract character zero from ASCII character
	addi $t1, $t1, -48
	# multiple sum * 10
	mult $v0, $t9
	mflo $v0
	# Add c - '0' to the sum
	add $v0, $v0, $t1
	# increment the address by 1
	addi $t0, $t0, 1
	j atoi_loop
atoi_sign:
	# Check to see if we need to negate the number
	bne $t8, 1, atoi_done
	# else we need to negate the final value
	not $v0, $v0			# Flip all the bits
	addi $v0, $v0, 1		# Add 1
atoi_done:
	jr $ra
#END ASCII TO INTEGER


#STRING LENGTH
strlen:
	#starting address of string in $a0
	add $t8, $a0, $0	#copy the address
	add $v0, $0, $0		#initialize count 
strlen_loop:
	lb $t0, 0($t8)		#load first char of string
	beqz $t0, end_strlen	#if NULL char, stop counting
	addi $v0, $v0, 1	# count++
	addi $t8, $t8, 1	# move address to next byte
	j strlen_loop
end_strlen:
	jr $ra
#END STRING LENGTH

#CUT STRING
cut:
	#Define your code here
	# The start of the string will be stored in $t0
	# The pointer that will move along the string will be stored in $t7
	# The pointer to the start pattern will be stored in $t1
	# The pointer used to iterate through the start pattern will be stored in $t8
	# The pointer to the end pattern will be stored in $t2
	# The pointer used to iterate through the end pattern will be stored in $t9
	# DUMMY variables will be stored in $t3 thru $t6. They will be used for comparisons
	
	#Init default return
	move $v0, $zero
	li $v1, -1
	
	#Init everything listed above
	move $t0, $a0
	move $t7, $a0
	move $t1, $a1
	move $t8, $a1
	move $t2, $a2
	move $t9, $a2
	
	
cut_check_start_loop:
	lb $t3, 0($t7)	#Load the character from the src string
	lb $t4, 0($t8)	#Load the character from the start pattern string
	beq $t3, $t4, cut_check_start_inner #If the first character matches, then jump to a loop to check the next character.
	beqz $t3, cut_end	# If the character we're checking is null, then we've reached the end of src and we have not found our character.
	addi $t7, $t7, 1	#Increment
	
	j cut_check_start_loop
	
cut_check_start_inner:
	#Here $t5 will store the original location of where we matched the string so we may jump back to it if necessary
	move $t5, $t7
	move $t6, $t8
	
cut_check_start_inner_loop:
	#Increment both pointers
	addi $t7, $t7, 1
	addi $t8, $t8, 1
	lb $t3, 0($t7)	#Load the character from the src string
	lb $t4, 0($t8)	#Load the character from the start pattern string

	beqz $t4, cut_check_end_loop	
	bne $t3, $t4, cut_return_to_outer_start_loop

	
	j cut_check_start_inner_loop
	
cut_return_to_outer_start_loop:
	move $t7, $t5
	addi $t7, $t7, 1
	move $t8, $t6
	j cut_check_start_loop	


cut_check_end_loop:
	lb $t3, 0($t7)	#Load the character from the src string
	lb $t4, 0($t9)	#Load the character from the end pattern string
	beq $t3, $t4, cut_check_end_inner #If the first character matches, then jump to a loop to check the next character.
	beqz $t3, cut_end # If the character we're checking is null, then we've reached the end of src and we have not found our pattern.
	addi $t7, $t7, 1
	
	j cut_check_end_loop
	
cut_check_end_inner:
	#Here $t6 will store the original location of where we matched the string so we may jump back to it if necessary
	move $t6, $t7
	move $t8, $t9
	
cut_check_end_inner_loop:
	#Increment both pointers
	addi $t7, $t7, 1
	addi $t9, $t9, 1
	lb $t3, 0($t7)	#Load the character from the src string
	lb $t4, 0($t9)	#Load the character from the start pattern string

	beqz $t4, PATTERN_FOUND	
	bne $t3, $t4, cut_return_to_outer_end_loop

	
	j cut_check_end_inner_loop

cut_return_to_outer_end_loop:
	move $t7, $t6
	addi $t7, $t7, 1
	move $t9, $t8
	j cut_check_end_loop	
	
PATTERN_FOUND:
	#We need to call strlen so save stuff onto the stack
	addi $sp, $sp, -16
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $s0, 8($sp)
	sw $s1, 12($sp)
	
	#move the things we need in to saved registers
	move $s0, $t5
	move $s1, $t6
	
	#load the end pattern into the string
	move $a0, $t2
	
	jal strlen
	
	#Move stuff back to where we had it so we don't break our code (this is bad form)
	move $t5, $s0
	move $t6, $s1
	
	#$t6 previously contained the start location of the end pattern but now we need the end location.
	add $t6, $t6, $v0
	
	#Load final values into return values
	move $v0, $t5
	sub $v1, $t6, $t5


	lw $s1, 12($sp)
	lw $s0, 8($sp)
	lw $a0, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 16
cut_end:
	jr $ra
#END CUT STRING



#STRING TOKENIZER
strtok:
	#Define your code here
	
	# The address of the string to be searched will be stored in $t0. This is also the address that will be returned.
	# The pointer that will step through the code will be located in $t1
	# The delimiter will be stored in $t2
	# The character being checked will be stored in $t3
	
	#Get the Delimiter
	move $t2, $a1
	
	#Check if the pointer is 0. This means use the same string.
	beqz $a0, strtok_input_addr_zero
	
	#If the pointer is not 0, just jump straight to the body.
	move $t0, $a0
	j strtok_body
	
strtok_input_addr_zero:
	#The input was null so we check the address of the pointer.
	lw $t0, ptr
	
	#If that pointer is 0, then we simply want to return nothing.
	beqz $t0, strtok_return_nothing
	
strtok_body:
	#Establish the pointer's starting location
	move $t1, $t0

strtok_loop:
	#Get the byte to be checked
	lb $t3, 0($t1)
	
	#End the function if we hit a '\0' terminator
	beqz $t3, strtok_delim_not_found
	
	#End the function if we find a delimiter
	beq $t3, $t2, strtok_delim_found
	
	#Increment our spot in the string
	addi $t1, $t1, 1
	
	j strtok_loop
	
	############## Remove these lines of code. These lines are only here to allow for main to continue working ###########
	#add $v0, $0, $0   #this makes strtok return a NULL
	###################################################################################################	

strtok_delim_found:
	
	
	#Check for repeated delimiters
strtok_repeated_delims:
	#Insert '\0' character
	sb $zero, 0($t1)
	#Increment by 1 to get start of next token
	addi $t1, $t1,1
	lb $t3, 0($t1)
	beq $t3, $t2, strtok_repeated_delims
	
	#Save the location of $t1 in ptr
	sw $t1, ptr
	
	#Return the location of the string
	move $v0, $t0
	
	#End the function
	j strtok_end
	
strtok_delim_not_found:
	#If we hit the end of the string, the last token. Store 0 in ptr so we know not to go through the string again.
	sw $zero, ptr
	move $v0, $t0
	j strtok_end
	
strtok_return_nothing:
	move $v0, $zero
	
strtok_end:
	jr $ra
#END STRING TOKENIZER
