# HW3 main file
# You are not submitting this file. Any changes needed for your HW assignment
# must be made in hw3.asm
.data

boot_msg_str1: .asciiz "WELCOME TO CSE 220\\n"
boot_msg_str2: .asciiz "\0"

menu_header: .asciiz "#### CSE220 VT100 simulator keyboard interface ####\n"
menu_msg: .asciiz "Format: OPTION_NUM ARGUMENTS\n\tprint: 0 string_to_print\n\tprint position: 1 x y string_to_print\n\tclear screen: 2\n\tEC 220 Logo: 3\n\tThis help menu: ?\n\tQuit: q\n\n\n"
menu_prompt: .asciiz "> "
menu_prompt_bad_input: .asciiz "Invalid input: "
menu_help: .asciiz "#### Help Menu ####\n"

debug_printk: .asciiz "PRINTK\n"
debug_printk_xy: .asciiz "PRINTK_XY\n"
debug_clear: .asciiz "CLEAR\n"
debug_ec: .asciiz "EXTRA CREDIT\n"

input_buffer: .space 1024

main_jmp_table: .word prompt_printk, prompt_printk_xy, prompt_clear, prompt_ec, prompt_help, prompt_end, prompt_continue

# syscalls
.eqv SYS_PRINT_INT 1
.eqv SYS_PRINT 4
.eqv SYS_READ 8
.eqv SYS_EXIT 10
.eqv SYS_PRINT_C 11

# constants
.eqv TRUE 1
.eqv FALSE 0
.eqv NULL 0x0

# Menu options
.eqv OPTION_PRINTK 0
.eqv OPTION_PRINTK_XY 1
.eqv OPTION_CLEAR 2
.eqv OPTION_EC 3
.eqv OPTION_HELP 4
.eqv OPTION_QUIT 5
.eqv OPTION_EMPTY 6

# Begin Helper macros

.macro print_str(%string)
	li $v0, SYS_PRINT
	la $a0, %string
	syscall
.end_macro

.macro print_char(%char)
	li $v0, SYS_PRINT_C
	la $a0, %char
	syscall
.end_macro

.macro print_int(%reg)
	li $v0, SYS_PRINT_INT
	move $a0, %reg
	syscall 
.end_macro

.macro get_input(%buffer, %length)
	li $v0, SYS_READ
	la $a0, %buffer
	li $a1, %length
	syscall
.end_macro

.text
.globl main
main:
	# Display the boot message
	jal boot_msg
	# Drop into the input menu
	print_str(menu_header)
	print_str(menu_msg)
	# Start the prompt that waits for input
prompt:
	print_str(menu_prompt)
	get_input(input_buffer, 1024)
	# Evaluate the input
	la $a0, input_buffer
	jal eval_input	
	# Check if it was successful
	beqz $v0, prompt_bad_input
	# We had a correct option given. Calculate the jump table address
	la $t0, main_jmp_table	# Load the jump table address
	li $t1, 4				# multiply the option value by 4
	mult $v1, $t1
	mflo $t1
	# Add the offset for the option to the base address of the jump table
	add $t0, $t0, $t1 
	# Load the address from the value in the jump table
	lw $t0, 0($t0)
	# now jump to the option and hope its right :D
	jr $t0
	# Should never get here
	# j prompt_continue

# Jump table labels start here
prompt_help:
	print_str(menu_help)
	print_str(menu_msg)
	j prompt_continue
	
prompt_bad_input:
	# Tell user bad input
	print_str(menu_prompt_bad_input)
	# Show user what they input
	print_str(input_buffer)
	# Show user help menu
	print_str(menu_msg)
	j prompt_continue

prompt_printk:
	print_str(debug_printk)
	# We assume by the validation function that
	# there is at minimum the option and a space
	la $a0, input_buffer
	addi $a0, $a0, 2		# This is the position after '0 '
	jal printk
	# Return back to the main loop
	j prompt_continue
	
prompt_printk_xy:
	print_str(debug_printk_xy)
	# Parse (x,y) position
	la $a0, input_buffer
	jal extract_coords
	# Check to make sure that we have valid returns
	bltz $v0, prompt_continue		# if $v0 is -1 we enetered an incorrect value	
	# Everything was ok so far. Move pointer to start of string to print.
	la $a0, input_buffer
	# There should be 3 spaces until the start of the string
	li $t0, 0
prompt_printk_xy_loop:
	lbu $t1, 0($a0)				# Get the value
	beqz $t1, prompt_continue	# We found a '\0' before getting to the start of the string
	addi $a0, $a0, 1			# Add 1 to the address
	bne $t1, ' ', prompt_printk_xy_loop	# if its not a space loop again
	addi $t0, $t0, 1				# it was a space so mark that we found 1
	beq $t0, 3, prompt_printk_xy_continue	# Found 3 spaces. Should be at the right spot
	j prompt_printk_xy_loop
prompt_printk_xy_continue:
	# Call printk_xy
	# $a0 already has the address of the string
	move $a1, $v0
	move $a2, $v1
	jal printk_xy
	
	j prompt_continue

prompt_clear:
	# Clear the screen.
	print_str(debug_clear)
	jal clear
	j prompt_continue
	
prompt_ec:
	# Print out the extra credit ASCII art
	print_str(debug_ec)
	jal ec
	j prompt_continue

prompt_continue:
	j prompt

prompt_end:
	li $v0, SYS_EXIT
	syscall

# Simple function for printing out the boot message
boot_msg:
	addi $sp, $sp, -4
	sw $ra, 0($sp)
	# Print out first msg
	la $a0, boot_msg_str1
	jal printk
	# Print out the second message
	la $a0, boot_msg_str2
	jal printk
	# Restore the return address
	lw $ra, 0($sp)
	addi $sp, $sp, 4
	# Return
	jr $ra

# Evaluates that a correct input option was provided.
# @param $a0 Contains the address of the input buffer
# @return1 $v0 Returns TRUE if the input option was correct, else FALSE.
# @return2 $v1 Returns the input option provided. Only valid if $v0 is TRUE.
eval_input:	
	li $v0, FALSE	# Default the return value to FALSE
	# Make sure $a0 is not NULL (0x0)
	beqz $a0, eval_input_done
	# If we got here lets check the first byte to make sure its not an empty string
	lbu $t0, 0($a0)   # load the first byte
	# If the first byte is '\0' we are done; empty input given
	beqz $t0, eval_input_empty
	# if the first byte is '\n' we are done; empty input given
	li $t1, '\n'
	beq $t0, $t1, eval_input_empty
	# if the first byte is ' ' we are done; empty input given
	li $t1, ' '
	beq $t0, $t1, eval_input_empty
	# Check for the help option
	li $t1, '?'
	beq $t0, $t1, eval_input_help
	# Check for the quit option
	li $t1, 'q'
	beq $t0, $t1, eval_input_q
	# Check for the option [0,3]
	lbu $t0, 0($a0)
	blt $t0, '0', eval_input_done
	bgt $t0, '3', eval_input_done
	# If we got here, we have a valid input option in the range [0,3]
	addi $v1, $t0, -48 # Convert the character to '0' - '3' to an int [0,3]
	# Check options 0 and 1 to see if there is a space after
	beq $v1, OPTION_CLEAR, eval_input_true	# This option has no arguments so we are done 
	beq $v1, OPTION_EC, eval_input_true         # This option has no arguments so we are done
	# Load the next byte to see if there is a space
	lbu $t1, 1($a0)
	bne $t1, ' ', eval_input_done
	# If we got here the input was valid
	j eval_input_true	

eval_input_true:
	li $v0, TRUE		# Set valid input option
	j eval_input_done

eval_input_help:
	# Adjust the '?' value to an integer index
	# so we can use it for the jump table
	li $v1, OPTION_HELP
	j eval_input_true

eval_input_q:
	# Adjust the 'q' value to an integer index
	# so we can use it for the jump table
	li $v1, OPTION_QUIT
	j eval_input_true

eval_input_empty:
	# The user just pressed enter or space enter
	# so just say it was valid even though its not a command
	li $v1, OPTION_EMPTY
	j eval_input_true

eval_input_done:
	jr $ra 

# Extracts the (x,y) position from the input string.
# This function makes the assumption that the string is in the format '1 ' 
# because it passed the input validation method.
# @param $a0 Contains the address of the input buffer.
# @return1 $v0 Returns a valid x coordinate from [0,79] if valid, else -1.
# @return2 $v1 Returns a valid y coordinate from [0, 24] if valid, else -1.
extract_coords:
	addi $sp, $sp, -16
	sw $ra, 0($sp)
	sw $a0, 4($sp)
	sw $s0, 8($sp)
	sw $s1, 12($sp)
	# Check if the address is 0x0 (NULL)
	beqz $a0, extract_coords_done
	# Extract x-value
	move $s0, $a0	# $s0 stores the starting address of x
	# Move $a0 to the start of x
	addi $a0, $a0, 2
extract_coords_x:
	lbu $t0, 0($a0)
	# Make sure we have atleast 1 number value
	blt $t0, '0', extract_coords_fail 
	bgt $t0, '9', extract_coords_fail
	# Save $a0 = start of x on the stack
	addi $sp, $sp, -4
	sw $a0, 0($sp)
	# If we do use atoi to find the value
	# $a0 is already the address of x
	jal atoi
	move $s1, $v0	 # Save the X value in a $s1 for now
	# Take the start of X address off the stack
	lw $a0, 0($sp)
	addi $sp, $sp, 4
	# Now move the address to the Y coordinate
extract_coords_move_to_y:
	lbu $t0, 0($a0)
	beq $t0, ' ', extract_coords_y		# If we found the next space we are @ the start of y
	# If we got this far, check to see if this is atleast a digit [0, 9]
	blt $t0, '0', extract_coords_fail
	bgt $t0, '9', extract_coords_fail
	# If we got here then we have a vaild digit.
	addi $a0, $a0, 1
	j extract_coords_move_to_y
	
extract_coords_y:
	# Address should currently be at a space character so add 1 to it to start @ y digit
	addi $a0, $a0, 1
	# Check to make sure there is atleast one digit in y
	lbu $t0, 0($a0)
	blt $t0, '0', extract_coords_fail
	bgt $t0, '9', extract_coords_fail
	# We got here so atleast 1 digit
	jal atoi
	move $v1, $v0		# Store the Y-Value in $v1
	move $v0, $s1		# Store the X-Value in $v0
	# Make sure we have a valid coordinate [0, 79] in x
	bltz $v0, extract_coords_fail
	bgt $v0, 79, extract_coords_fail
	# Make sure we have a valid coordinate [0, 24] in y
	bltz $v1, extract_coords_fail
	bgt $v1, 24, extract_coords_fail
	# If we got this far everything is vaild
	j extract_coords_done
		
extract_coords_fail:
	# Set both return values to -1
	li $v0, -1
	li $v1, -1
	j extract_coords_done
	
extract_coords_done:
	lw $s1, 12($sp)
	lw $s0, 8($sp)
	lw $a0, 4($sp)
	lw $ra, 0($sp)
	addi $sp, $sp, 16
	jr $ra

#################################################################

# Student defined functions will be included starting here

#################################################################

.include "hw3.asm"
